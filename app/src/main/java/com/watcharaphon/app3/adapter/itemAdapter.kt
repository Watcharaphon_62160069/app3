package com.watcharaphon.app3.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.watcharaphon.app3.R
import com.watcharaphon.app3.model.Oil

class ItemAdapter(
    private val context: Context,
    private val dataset: List<Oil>
) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.findViewById(R.id.OilName)
        val priceTextView: TextView = view.findViewById(R.id.Price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.nameTextView.text = item.Name
        holder.priceTextView.text = item.price.toString()

        holder.itemView.setOnClickListener{
            Toast.makeText(context,item.price.toString() + " " + item.Name,Toast.LENGTH_SHORT).show()
        }
    }
}