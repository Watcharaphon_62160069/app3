package com.watcharaphon.app3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.watcharaphon.app3.adapter.ItemAdapter
import com.watcharaphon.app3.data.dataSource

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myDataset = dataSource().loadData()
        val recycleView = findViewById<RecyclerView>(R.id.recycleView)
        recycleView.adapter = ItemAdapter(this,myDataset)
        recycleView.setHasFixedSize(true)
    }
}