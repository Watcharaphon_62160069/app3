package com.watcharaphon.app3.data

import com.watcharaphon.app3.model.Oil

class dataSource {
    fun loadData(): List<Oil>{
        return listOf<Oil>(
            Oil(36.84,"เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20") ,
            Oil(37.68,"เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91") ,
            Oil(37.95,"เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95") ,
            Oil(45.44,"เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95") ,
            Oil(36.34,"เชลล์ ดีเซล B20") ,
            Oil(36.34,"เชลล์ ฟิวเซฟ ดีเซล") ,
            Oil(36.34,"เชลล์ ฟิวเซฟ ดีเซล B7") ,
            Oil(36.34,"เชลล์ วี-เพาเวอร์ ดีเซล") ,
            Oil(47.06,"เชลล์ วี-เพาเวอร์ ดีเซล B7") ,


            )
    }
}